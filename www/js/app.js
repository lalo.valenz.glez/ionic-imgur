angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
  .state('imgur', {
    url: '/imgur',
        templateUrl: 'templates/pictures.html',
        controller: 'PicturesController'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/imgur');

});
