angular.module('starter')
.controller('PicturesController', function($scope, $http){
    $scope.imagenes = [];
    $scope.$on('$ionicView.beforeEnter', function(){
        $http.get('https://api.imgur.com/3/gallery/hot/viral/0').then(function(resp){
            for(var i in resp.data.data){
                var image = resp.data.data[i];
                if(!image.is_album) {
                    $scope.imagenes.push({
                        title: image.title,
                        description: image.desciption,
                        ups: image.ups,
                        downs: image.downs,
                        link: image.link
                    });
                }
            }
        }).catch(function (err) {
            console.log(err);
        });
    });
});